package ArrayInJava;

public class ThisKeywordInJava {

	int i = 10;

	public ThisKeywordInJava() {
		this(345);
//		i=78;
	}
	public ThisKeywordInJava(int i) {
		this.i = i;
	}
	public void print() {
		System.out.println(i);
		int i = 78;
		System.out.println(i);
		System.out.println(this.i);
		this.i = 45;
		i = 66;
	}
	public void show() {
		System.out.println(i);
	}
	public static void main(String[] args) {
//		ThisKeywordInJava dimpy = new ThisKeywordInJava(34);
		ThisKeywordInJava rounak = new ThisKeywordInJava();
//		dimpy.print();
//		dimpy.show();
		rounak.show();
	}
}
