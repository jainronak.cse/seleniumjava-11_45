package ArrayInJava;

public class Programs3 {

	public static void main(String[] args) {

		int a = 10;
		int b = 20;
		int c;

		System.out.println(a + " " + b);
//		using three
//		c = b;
//		b = a;
//		a = c;

//		using two
		a = a + b;
		b = a - b;
		a = a - b;

		System.out.println(a + " " + b);

	}

}
